
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.14159265

int main ()
{
   double x, val1, val2, val;
   double sq1, sq2 , result ;

   val = PI / 180.0;
   

   printf("introduza em graus um valor qualquer\n");
   scanf("%lf", &x);

   val1 = cos( x*val );
   val2 = sin( x*val );

 	sq1 = val1 * val1;
 	sq2 = val2 * val2;

 	result = sq2 + sq1;

   printf("cos( %lf) = %lf\n", x, val1);
   printf("sen( %lf) = %lf\n", x, val2);
   printf(" quadrado do cos %lf e do sen %lf\n", sq1, sq2);
   printf(" resultado de tudo isto é %lf\n", result);
   

 
   return 0;
}